Instruction to Participants
===========================

Prerequisite
------------

You need to have a JDK installed. Ensure ```JAVA_HOME``` is set in your environment.
You also need to have ```git``` installed. If you prefer not to have ```git``` on your system,
please follow the instructions further down.

Please have this set-up and the below instructions followed. You might also have to refresh
the code base the day before the conference.

Instructions for anyone but Windows users
-----------------------------------------

In a shell do:

```
git clone https://bitbucket.org/ysb33r/devsummer2014.git
cd DevSummer2014/exercises
./gradlew --info bootstrap
```

Instructions for Windows users
------------------------------

In a DOS/CMD prompt do:

```
git clone https://bitbucket.org/ysb33r/devsummer2014.git
cd DevSummer2014\exercises
gradlew.bat --info bootstrap
```

Instructions for those without git
----------------------------------

* Download the zip source from https://bitbucket.org/ysb33r/devsummer2014/get/master.zip.
* Unpack it and change into the ```exercises``` directory.
* Run ```./gradlew --info bootstrap``` or ```gradlew.bat --info bootstrap``` in a console.

How do I know it worked?
------------------------

Whilst ```gradlew``` is running ```bootstrap``` you will see a lot of downloading going on the first time. It will then 
proceed to actually build a small document. The output will hopefully look something like

https://bitbucket.org/ysb33r/devsummer2014/src/2f679d8cc0b4a993df0f7c0a718422923e1a7e01/bootstrap.png

When it completed it will print a link to local file. Open that file in a browser and it looks like the below,
success has been achieved!

https://bitbucket.org/ysb33r/devsummer2014/src/2f679d8cc0b4a993df0f7c0a718422923e1a7e01/bootstrap-browser.png


Problems?
---------

Send me a tweet - I am known as ```@ysb33r```.