# External Sources

Code in these subfolders are not necessarily under the same license
as the rest of this project. Please see the respective sites for the appropriate
license agreements.

* [deck.js](https://github.com/imakewebthings/deck.js)
* [asciidoctor-backends](https://github.com/asciidoctor/asciidoctor-backends)