== Getting Started

* Sections
** ++=++, ++==++, ++===++ etc.
** Must be at beginning of line
** A document usually only has *one* +level zero+ section, unless it is a ++:book:++

```
= Level 0
:author: Schalk Cronjé
:revnumber: 1.0

== Level 1

Let's put some text here
```

== Getting Started

* Unordered lists
** Use ++*++ or ++-++ for simple lists
** Use ++$$*$$++, ++$$**$$++, ++$$***$$++ etc. for nested lists
* Ordered lists
** Use +$$.$$+, ++$$..$$++, ++$$...$$++ etc. for nesting

```
* My plain list
** Use ++*++ or ++-++ for simple lists

. One
. Two
.. Two dot One
. Three

```

== Getting Started

.Checkboxes
```
- [*] Checked
- [ ] Unchecked
```

- [*] Checked
- [ ] Unchecked

== Getting Started

.Labeled Lists
Use +::+ to have indented lists with labels (works well in most backends)

```
Testable:: If the user acceptance criteria is not testable it cannot be built with confidence.

Traceable:: Who wants this requirement and why is it needed, what business strategy does it support?
```

Testable:: If the user acceptance criteria is not testable it cannot be built with confidence.

== Getting Started

Try this ...

```
What is the answer to L, U & E?::
  42
```

then this ...

```
[qanda]
What is the answer to L, U & E?::
  42
```
