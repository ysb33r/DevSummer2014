@Grapes([
        @Grab( 'org.ysb33r.groovy:groovy-vfs:0.5' ),
        @Grab( 'commons-httpclient:commons-httpclient:3.1')
        @Grab( 'com.jcraft:jsch:0.1.48' )
])
import org.ysb33r.groovy.dsl.vfs.VFS

def vfs = new VFS()

vfs {
    cp 'http://first.example/myfile', 'sftp://second.example/yourfile'
}